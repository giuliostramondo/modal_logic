%option noyywrap

%{
#include "collections.h"
#include "struct.h"
#include "modal-parser.tab.h"
#define UNKNOWN -1
#define DEB 0

extern int line_num;
%}

DIGIT [0-9]
ID       [a-uw-zA-Z_][a-uw-zA-Z0-9_]*

%%


"\r\n"            { ++line_num; }
"\n"              { ++line_num; }

[ \t\f\v]+        { /* Ignore whitespace. */ }

"//"[^\n]*        { ++line_num; /* ignore comment lines */ }

"[]"  {if ( DEB ) if ( DEB ) printf("token []\n"); return NEC;}
"<>"  {if ( DEB ) printf("token <>\n"); return POS;}
"^"   {if ( DEB ) printf("token ^\n"); return AND;}
"v"   {if ( DEB ) printf("token v\n");return OR;}
"->"  {if ( DEB ) printf("token ->\n");return IMPL;}
"<->" {if ( DEB ) printf("token <->\n");return IFF;}
"~"   {if ( DEB ) printf("token ~\n");return NOT;}
"("   {if ( DEB ) printf("token (\n");return L_PAR; }
")"   {if ( DEB ) printf("token )\n");return R_PAR; }

"="   {if ( DEB ) printf("token =\n");return EQ; }
"{"   {if ( DEB ) printf("token {\n");return L_BRA;}
"}"	  {if ( DEB ) printf("token }\n");return R_BRA;}
","   {if ( DEB ) printf("token ,\n");return COMMA;}
";"	  {if ( DEB ) printf("token ;\n");return SEMI;}
"|="  {if ( DEB ) printf("token |=\n");return DERIVE;}

"DECLARATIONS"   {if ( DEB ) printf("token DECLARATIONS\n");return DECL;}
"COMPUTATIONS"   {if ( DEB ) printf("token COMPUTATIONS\n");return COMP;}

"Predicate_set" {if ( DEB ) printf("token Predicate_set\n");return PS;}
"World_set"     {if ( DEB ) printf("token World_set\n");return WS;}
"Eval_fun"      {if ( DEB ) printf("token Eval_fun\n");return EF;}
"Relation"      {if ( DEB ) printf("token Relation\n");return REL;}
"Frame"         {if ( DEB ) printf("token Frame\n");return FRA;}
"Model"         {if ( DEB ) printf("token Model\n");return MOD;}

{ID} {if ( DEB ) printf("token ID\n"); yylval.svalue=strdup(yytext); return ID; }
. {
	yyerror("Unknown char");
	return UNKNOWN;
	}

