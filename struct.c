#include "struct.h"

t_model *alloc_model(char *identifier, t_declaration *w, t_declaration *r, t_declaration *eval,t_declaration *predicates){
	t_model *result;
	result = (t_model *) malloc(sizeof(t_model));
	if(result==NULL) return NULL;

	result->identifier=identifier;
	result->world_set=w;
	result->relation=r;
	result->evaluation=eval;
	result->predicates=predicates;
	return result;
}

t_couple *alloc_couple(char *item1, char *item2){
	t_couple *result;
	result =(t_couple *) malloc(sizeof(t_couple));
	if(result==NULL) return NULL;
	result->item1=item1;
	result->item2=item2;
	return result;
}

t_expression *alloc_expression(char *world, t_model *model){
	t_expression *result;
	result =(t_expression *) malloc(sizeof(t_expression));
	if(result==NULL) return NULL;
	result->world=world;
	result->model=model;
	return result; 
}

t_declaration *alloc_declaration(char *identifier, int isType, t_list *elements){
	t_declaration *result;

	result = (t_declaration *) malloc(sizeof(t_declaration));
	if (result == NULL) return NULL;

	result->identifier = identifier;
	if(isType == 1){
	result->isWorld=1;
	result->isPredicate=0;
	result->isEvaluation=0;
	result->isEvaluationItem=0;
	result->isRelation=0;
	}
	else if(isType == 0){
	result->isWorld=0;
	result->isPredicate=1;
	result->isEvaluation=0;
	result->isEvaluationItem=0;
	result->isRelation=0;
	}
	else if(isType == 2){
	result->isWorld=0;
	result->isPredicate=0;
	result->isEvaluation=1;
	result->isEvaluationItem=0;
	result->isRelation=0;
	}
	else if(isType == 3){
	result->isWorld=0;
	result->isPredicate=0;
	result->isEvaluation=0;
	result->isEvaluationItem=1;
	result->isRelation=0;
	}
	else if(isType == 4){
	result->isWorld=0;
	result->isPredicate=0;
	result->isEvaluation=0;
	result->isEvaluationItem=0;
	result->isRelation=1;
	}
	result->elements=elements;
	return result;
}