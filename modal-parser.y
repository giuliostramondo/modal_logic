%{
#include <stdio.h>    
#include <stdarg.h> 
#include <stdlib.h>
#include <assert.h>
#include <malloc/malloc.h>
#include "collections.h"
#include "struct.h"
#define DEBUG 0
int line_num;

t_list *world_sets = NULL; //list of t_declarations, identifier is the world set name, elements is t_list of world's identifiers
t_list *predicate_sets = NULL; //list of t_declarations, identifier is predicate set name, elements t_list of predicate's identifiers
t_list *eval_func = NULL;  // list of t_declarations, each t_declaration has as elements an other list
                    // of t_declarations containing world as identifier, id list of predicates true.
t_list *relations = NULL;
t_list *models = NULL;

t_model *queryModel=NULL;
char*	 queryWorld=NULL;


int compareDeclarations (void *Wor1, void *Wor2){
	t_declaration *w1;
	t_declaration *w2;

	if(Wor1 == NULL){
		if(Wor2 == NULL)
			return 1;
	}

	if(Wor2 == NULL) return 0;

	w1 = (t_declaration *) Wor1;
	w2 = (t_declaration *) Wor2;
	return (!strcmp(w1->identifier,w2->identifier));
}

int compareModels (void *Mod1, void *Mod2){
	t_declaration *m1;
	t_declaration *m2;

	if(Mod1 == NULL){
		if(Mod2 == NULL)
			return 1;
	}

	if(Mod2 == NULL) return 0;

	m1 = (t_declaration *) Mod1;
	m2 = (t_declaration *) Mod2;
	return (!strcmp(m1->identifier,m2->identifier));
}


t_model *getModel(char *identifier,t_list *list){
	t_model search_pattern;
	t_list *elementFound;

	if(identifier == NULL)
		printf("Trying to get a model from models\nbut given identifier is NULL\n");

	search_pattern.identifier=identifier;

	elementFound = CustomfindElement
			(list, &search_pattern,compareModels);

	if (elementFound != NULL)
		return (t_model *) LDATA(elementFound);

	return NULL;
}

void printDebug(const char * restrict format, ... ){

if(DEBUG){
	    va_list args;
    	va_start(args, format);
		vprintf(format,args);
		va_end(args);
	}
}

t_declaration *getDeclaration(char *identifier,t_list *list){
	t_declaration search_pattern;
	t_list *elementFound;

	if(identifier == NULL)
		printf("Trying to get a world from world_sets\nbut given identifier is NULL\n");

	search_pattern.identifier=identifier;

	elementFound = CustomfindElement
			(list, &search_pattern,compareDeclarations);

	if (elementFound != NULL)
		return (t_declaration *) LDATA(elementFound);

	return NULL;
}

char *getIdentifier_At(unsigned int position,t_list *list){
	t_list *elementFound;
	elementFound=getElementAt(list,position);

	if (elementFound != NULL)
		return (char *) LDATA(elementFound);

	return NULL;
}

int *getInteger_At(unsigned int position,t_list *list){
	t_list *elementFound;
	elementFound=getElementAt(list,position);

		if (elementFound != NULL)
		return (int *) LDATA(elementFound);

	return NULL;
}

int containsIdentifier(char *search_pattern, t_list *list_of_ids){
	char* tmp_item;
	unsigned int i=0;

	for (i = 0 ; i< getLength(list_of_ids);i++){
		tmp_item = getIdentifier_At(i,list_of_ids);
		if(tmp_item==NULL) return 0;
		if(!strcmp(search_pattern,tmp_item)) {return 1;}
	}
	return 0;
}

unsigned int getPositionFromID(char *search_pattern, t_list *list_of_ids){
	char *tmp_item;
	unsigned int i=0;

	for(i=0;i< getLength(list_of_ids);i++){
		tmp_item = getIdentifier_At(i,list_of_ids);
		if(tmp_item==NULL) return -1;
		if(!strcmp(search_pattern,tmp_item)) {return i;}
	}
	return -1;
}

t_couple *getCouple_At(unsigned int position, t_list *list){
		t_list *elementFound;
	elementFound=getElementAt(list,position);

	if (elementFound != NULL)
		return (t_couple *) LDATA(elementFound);

	return NULL;
}

t_list *getLinkList(char *world_identifier, t_list *relation){

	unsigned int i=0;
	t_couple *tmp_couple;
	t_list *result=NULL;

	for(i =0; i< getLength(relation);i++){
		tmp_couple = getCouple_At(i,relation);

		if(!strcmp(world_identifier,tmp_couple->item1)){ result=addElement(result,tmp_couple->item2,-1);}
	}
	return result;
}

t_declaration *getDeclaration_At(unsigned int position,t_list *list){
	t_list *elementFound;
	elementFound=getElementAt(list,position);

	if (elementFound != NULL)
		return (t_declaration *) LDATA(elementFound);

	return NULL;
}

t_model *getModel_At(unsigned int position,t_list *list){
	t_list *elementFound;
	elementFound=getElementAt(list,position);

	if (elementFound != NULL)
		return (t_model *) LDATA(elementFound);

	return NULL;
}

void printWorldSet(t_declaration *world_set){
		char* tmp_identifier;
		unsigned int i=0;
		printf("World set %s: { ",world_set->identifier);
		for(i=0;i<getLength(world_set->elements);i++){
			if(i!=getLength(world_set->elements)-1)
			printf("%s, ",getIdentifier_At(i,world_set->elements));
			else
			printf("%s ",getIdentifier_At(i,world_set->elements));
		}
		printf("}\n");
		return;
}

void printRelation(t_declaration *relation){
	char* tmp_identifier1;
	char* tmp_identifier2;
	t_couple *tmp_couple;
	unsigned int i=0;
	printf("Relation %s: { ",relation->identifier );
	for(i=0;i<getLength(relation->elements);i++){
		tmp_couple=getCouple_At(i,relation->elements);
		if(i!=getLength(relation->elements)-1)
		printf("( %s, %s ), ",tmp_couple->item1, tmp_couple->item2);
		else
		printf("( %s, %s ) ",tmp_couple->item1, tmp_couple->item2);
	}
	printf("}\n");

}

void printEvaluationFunction(t_declaration *eval_func){
	t_declaration *eval_item;
	char *predicate_item;
	unsigned int i=0;
	unsigned int j=0;
	printf("Evaluation Function %s: { ",eval_func->identifier);
	for(i=0;i<getLength(eval_func->elements);i++){
		eval_item=getDeclaration_At(i, eval_func->elements);
		printf("( %s, ( ",eval_item->identifier);
			for(j=0;j<getLength(eval_item->elements);j++){
				predicate_item=getIdentifier_At(j,eval_item->elements);
				if(j!=getLength(eval_item->elements)-1)
					printf("%s, ",predicate_item);
				else
					printf("%s ",predicate_item);
			}
		if(i!=getLength(eval_func->elements)-1)
			printf(") ), ");
		else 
			printf(") ) ");
	}
	printf(" }\n");


}

void printPredicateSet(t_declaration *predicate_set){
		char *predicate_item;
		unsigned int i=0;
		printf("Predicate Set %s: { ",predicate_set->identifier);
		for (i=0;i<getLength(predicate_set->elements);i++){
			predicate_item=getIdentifier_At(i,predicate_set->elements);
			if( i!= getLength(predicate_set->elements) -1)
				printf("%s, ",predicate_item );
			else
				printf("%s ", predicate_item );
		}
		printf("} \n");
}

void printModel(t_model *tmp_model){
				t_declaration *tmp_world_set=NULL;
				t_declaration *tmp_pred_set=NULL;
				t_declaration *tmp_rel=NULL;
				t_declaration *tmp_eval=NULL;

				tmp_world_set=tmp_model->world_set;
				tmp_eval=tmp_model->evaluation;
				tmp_pred_set=tmp_model->predicates;
				tmp_rel=tmp_model->relation;

			printf("Model ID: %s\n", tmp_model->identifier);
			printWorldSet(tmp_world_set);
			printRelation(tmp_rel);
			printEvaluationFunction(tmp_eval);
			printPredicateSet(tmp_pred_set);
			
return;
}


%}


//------------------------------
//			Record Semantici
//______________________________
%union {
	char *svalue;
	t_list *list;
	int intval;
	t_couple *couple;
	t_declaration *declaration;
}

//------------------------------
//			Tokens
//______________________________

%start program

%token <svalue> ID
%token EQ
%token L_BRA
%token R_BRA
%token COMMA
%token SEMI
%token DECL
%token COMP
%token PS
%token WS
%token EF
%token REL
%token FRA
%token MOD
%token DERIVE

%type <list> id_list
%type <declaration> eval_item
%type <list> eval_item_list
%type <list> rel_item_list
%type <couple> rel_item
%type <list> exp

//------------------------
//      Precedenze
//------------------------

%left IFF
%left IMPL
%left OR
%left AND
%right NOT NEC POS
%left L_PAR R_PAR
%%

program: model_declaration calculi
{
		if(DEBUG){
			printf("%d Models, %d Predicate sets, %d Evaluation functions, %d Relations, %d World sets.\n",
					getLength(models),getLength(predicate_sets),getLength(eval_func),getLength(relations),getLength(world_sets));

			unsigned int i=0;
			t_model *tmp_model=NULL;
			for(i=0;i<getLength(models);i++){
				tmp_model=getModel_At(i,models);
				printf("Model %d\n",i);
				printModel(tmp_model);
			}
		}
            /* return from yyparse() */
            YYACCEPT;

         }
;

model_declaration: DECL { printDebug ("Parsed start of declarations\n"); fflush(stdout);} declarations { /* does nothing */ }
			;

declarations: declarations declaration { /* does nothing */ }
			| /* Empty */              { /* does nothing */ }
			;


declaration: predicates
			| worlds 
			| eval
			| relation
			| model
			;



predicates : PS ID EQ L_BRA id_list R_BRA SEMI { 
				printDebug("Parsed predicate %s declaration\n",$2); fflush(stdout);
				t_declaration *tmp_pred;
				tmp_pred = alloc_declaration($2,0,$5);
				predicate_sets = addElement(predicate_sets,tmp_pred,-1);
				}
			;

worlds : WS ID EQ L_BRA id_list R_BRA SEMI { 
				printDebug("Parsed world %s declaration\n",$2); fflush(stdout);
				t_declaration *tmp_pred;
				tmp_pred = alloc_declaration($2,1,$5);
				world_sets = addElement(world_sets,tmp_pred,-1);
				}
			;

eval : EF ID EQ L_BRA eval_item_list R_BRA SEMI { 
				printDebug("Parsed evaluation function %s\n",$2);fflush(stdout);
				t_declaration *tmp_pred;
				tmp_pred = alloc_declaration($2,2,$5);
				eval_func = addElement(eval_func,tmp_pred,-1);
				}
			;

eval_item_list : eval_item_list COMMA eval_item 
				{ 
					$$ = addElement($1,$3,-1);
				}
				| eval_item 
				{ 
					$$ = addElement(NULL, $1, -1);
				}
				;
			// ( identifier , ( identifier , identifier ) )
eval_item : L_PAR ID COMMA L_PAR id_list R_PAR R_PAR {
				t_declaration *tmp_pred;
				$$ = alloc_declaration($2,3,$5);
				 }
			;

id_list : id_list COMMA ID
		{
			// add element to list of id
			$$ = addElement($1, $3, -1);

		}
		| ID
		{
		 // add element to list
		  $$ = addElement(NULL, $1, -1);
		}
		;

relation : REL ID EQ L_BRA rel_item_list R_BRA SEMI { 
				printDebug("Parsed relation %s declaration\n",$2);fflush(stdout);
				t_declaration *tmp_rel;
				tmp_rel = alloc_declaration($2,4,$5);
				relations = addElement(relations,tmp_rel,-1);
				}
		;

rel_item_list : rel_item_list COMMA rel_item 
				{
					$$ = addElement($1,$3,-1);
				 }
			| rel_item 
			{
				$$ = addElement(NULL,$1,-1);
			 }
			;

rel_item : L_PAR ID COMMA ID R_PAR { 
			$$ = alloc_couple($2,$4);
			}
		;

model : MOD ID EQ L_PAR ID COMMA ID COMMA ID COMMA ID R_PAR SEMI
		{
			printDebug("Parsed model %s declaration\n",$2);
			t_declaration *world;
			t_declaration *relation;
			t_declaration *evaluation;
			t_declaration *predicates;

			world = getDeclaration($5,world_sets);
			relation = getDeclaration($7,relations);
			evaluation = getDeclaration($9,eval_func);
			predicates = getDeclaration($11,predicate_sets);


			if(world == NULL) printf("Delaration of Model %s:\nWorld %s hasn't been declared",$2,$5);
			if(relations == NULL) printf("Delaration of Model %s:\nRelation %s hasn't been declared",$2,$7);
			if(evaluation == NULL) printf("Delaration of Model %s:\nEvaluation Function %s hasn't been declared",$2,$9);
			if(predicates == NULL) printf("Delaration of Model %s:\nPredicate %s hasn't been declared",$2,$11);

			t_model *model;
			model = alloc_model($2,world,relation,evaluation,predicates);
			models = addElement(models,model,-1);
		}


calculi: /*Empty */		
		| calculi calculus
		;

calculus: 
		 ID DERIVE { queryModel = getModel($1,models); if(queryModel==NULL)printf("error: model not found\n");} exp {

		unsigned int i=0;
		int truth=1;
		for (int i = 0; i < getLength($4); ++i)
		{
			if(! (*getInteger_At(i,$4)))	truth = 0;
		}
		if(truth)printf("<<true>>\n");
		else printf("<<false>>\n");
		queryModel=NULL;
	}
	|
	ID COMMA ID DERIVE { queryModel = getModel($1,models); if(queryModel==NULL)printf("Error: model not found\n");} exp {
			t_declaration *world_set = queryModel->world_set;
			unsigned int i = getPositionFromID($3,world_set->elements);
			if(*getInteger_At(i,$6)) printf("<<true>>\n");
			else printf("<<false>>\n");
	} 
	;

//EXP has to be a list of integer (which are boolean)
//the list have to have an element for each world
//the parser has to evaluate the expression for all world at the same time
//otherwise it's impossible to evaluate [] and <>

exp: ID {
			t_declaration *eval_func = queryModel->evaluation;
			t_declaration *world_set = queryModel->world_set;
			t_declaration *tmp_world_truths =NULL;
			char *tmp_world_ID = NULL;
			int *tmp_result=NULL;
			t_list *exp_result=NULL;
			printDebug("checking truth of :%s\n",$1);

			unsigned int i=0;
			for(i=0; i<getLength(world_set->elements); i++){

				tmp_world_ID = getIdentifier_At(i,world_set->elements);

				tmp_world_truths = getDeclaration (tmp_world_ID,eval_func->elements);

				tmp_result = (int *) malloc(sizeof(int));
				if (tmp_world_truths == NULL){
					printDebug("world %s: false\n", tmp_world_ID); fflush(stdout);
					*tmp_result=0;
					exp_result=addElement(exp_result,tmp_result,-1);
					continue;
				} 

				*tmp_result = containsIdentifier($1,tmp_world_truths->elements);
				if(*tmp_result)
				printDebug("world %s: true\n",tmp_world_ID);
				else
				printDebug("world %s: false\n",tmp_world_ID);

				exp_result=addElement(exp_result,tmp_result,-1);
				
			}
			$$=exp_result;
		}
		| exp IFF exp     { 
																										//Maybe free the exps once you are done with them
							unsigned int i=0;
							int *tmp_result = NULL;
							t_list *exp_result=NULL;
							tmp_result = (int *) malloc(sizeof(int));

							int exp1,exp2;
							for (i=0; i< getLength($1);i++){
								exp1 = *(getInteger_At(i,$1));
								exp2 = *(getInteger_At(i,$3));
								*tmp_result = (!exp1)||(exp2);
								*tmp_result = (exp1&&exp2)||((!exp1)&&(!exp2));
								if(*tmp_result){printDebug("true\n");}
								else{printDebug("false\n");}

								exp_result=addElement(exp_result,tmp_result,-1);
							}
							$$=exp_result;
							}
		| exp IMPL exp    { 
																		//Maybe free the exps once you are done with them
							unsigned int i=0;
							int *tmp_result = NULL;
							t_list *exp_result=NULL;


							int exp1,exp2;
							for (i=0; i< getLength($1);i++){
								tmp_result = (int *) malloc(sizeof(int));
								exp1 = *(getInteger_At(i,$1));
								exp2 = *(getInteger_At(i,$3));
								*tmp_result = (!exp1)||(exp2);
								
								if(*tmp_result){printDebug("true\n");}
								else{printDebug("false\n");}

								exp_result=addElement(exp_result,tmp_result,-1);

							}
							
							$$=exp_result;
								}
		| exp OR exp      { 
															//Maybe free the exps once you are done with them
							unsigned int i=0;
							int *tmp_result = NULL;
							t_list *exp_result=NULL;
							tmp_result = (int *) malloc(sizeof(int));

							int exp1,exp2;
							for (i=0; i< getLength($1);i++){
								exp1 = *(getInteger_At(i,$1));
								exp2 = *(getInteger_At(i,$3));
								*tmp_result = exp1||exp2;
								if(*tmp_result){printDebug("true\n");}
								else{printDebug("false\n");}

								exp_result=addElement(exp_result,tmp_result,-1);
							}
							$$=exp_result;
							}
		| exp AND exp     { 
							//Maybe free the exps once you are done with them
							unsigned int i=0;
							int *tmp_result = NULL;
							t_list *exp_result=NULL;
							tmp_result = (int *) malloc(sizeof(int));

							int exp1,exp2;
							for (i=0; i< getLength($1);i++){
								exp1 = *(getInteger_At(i,$1));
								exp2 = *(getInteger_At(i,$3));
								*tmp_result = exp1&&exp2;
								if(*tmp_result){printDebug("true\n");}
								else{printDebug("false\n");}

								exp_result=addElement(exp_result,tmp_result,-1);
							}
							$$=exp_result;
							}

		| NOT exp         { 
							//Maybe free the exps once you are done with them
							unsigned int i=0;
							int *tmp_result = NULL;
							t_list *exp_result=NULL;
							tmp_result = (int *) malloc(sizeof(int));

							int exp1;
							for (i=0; i< getLength($2);i++){
								exp1 = *(getInteger_At(i,$2));
								*tmp_result = !exp1;
								if(*tmp_result){printDebug("true\n");}
								else{printDebug("false\n");}

								exp_result=addElement(exp_result,tmp_result,-1);
							}
							$$=exp_result;
								}
		| NEC exp         { 
								t_declaration *world_set = queryModel->world_set;
								t_declaration *relation = queryModel->relation;
								t_list * links;
								unsigned int i=0,j=0;
								char* tmp_world_ID=NULL;
								t_list *exp_result=NULL;
								int tmp_eval=1;
								unsigned int tmp_id;

								printDebug("Analyzing truth of operator []\n");
								printDebug("truth values recived:\n");

								for (i = 0; i < getLength($2); i++)
								{
									printDebug("position %d: ", i );
									if(*(getInteger_At(i,$2))){printDebug("true\n");}
									else printDebug("false\n");
								}
								for (i=0; i< getLength($2); i++){
									tmp_world_ID = getIdentifier_At(i,world_set->elements);
									printDebug("world %s is connected with:\n", tmp_world_ID);
									links = getLinkList(tmp_world_ID,relation->elements);

									tmp_eval=1;
									for (j=0;j<getLength(links);j++){
										char *toLink=getIdentifier_At(j,links);
										printDebug("world %s where expr is ",toLink );
										tmp_id=getPositionFromID(toLink,world_set->elements);
										printDebug("position: %d\n", tmp_id);
										int tmp=(*getInteger_At(tmp_id,$2));

										if(!(*getInteger_At(tmp_id,$2))){ printDebug("false\n");tmp_eval=0;}
										else printDebug("true\n");
									}
									int *tmp_res=(int *)malloc(sizeof(int));

									if(tmp_eval){
										*tmp_res=1;
										printDebug("so in this world [] exp is true\n");
									}
									else{										
										*tmp_res=0;
										printDebug("so in this world [] exp is false\n");
									}
									exp_result=addElement(exp_result,tmp_res,-1);
									
								}
									$$=exp_result;
								 }
		| POS exp         { 
								t_declaration *world_set = queryModel->world_set;
								t_declaration *relation = queryModel->relation;
								t_list * links;
								unsigned int i=0,j=0;
								char* tmp_world_ID=NULL;
								t_list *exp_result=NULL;
								int tmp_eval=1;
								unsigned int tmp_id;

								printDebug("Analyzing truth of operator <>\n");
								printDebug("truth values recived:->>>\n");

								for (i = 0; i < getLength($2); i++)
								{
									printDebug("position %d: ", i );
									printDebug("Debug: position %d: ", i);
									if(*(getInteger_At(i,$2))){printDebug("true\n");}
									else printDebug("false\n");
								}
								for (i=0; i< getLength($2); i++){
									tmp_world_ID = getIdentifier_At(i,world_set->elements);
									printDebug("world %s is connected with:\n", tmp_world_ID);
									links = getLinkList(tmp_world_ID,relation->elements);

									tmp_eval=0;
									for (j=0;j<getLength(links);j++){
										char *toLink=getIdentifier_At(j,links);
										printDebug("world %s where expr is ",toLink );
										tmp_id=getPositionFromID(toLink,world_set->elements);
										printDebug("position: %d\n", tmp_id);
										int tmp=(*getInteger_At(tmp_id,$2));

										if(*getInteger_At(tmp_id,$2)){ printDebug("true\n");tmp_eval=1;}
										else printDebug("false\n");
									}
									int *tmp_res=(int *)malloc(sizeof(int));

									if(tmp_eval){
										*tmp_res=1;
										printDebug("so in this world <> exp is true\n");
									}
									else{										
										*tmp_res=0;
										printDebug("so in this world <> exp is false\n");
									}
									exp_result=addElement(exp_result,tmp_res,-1);
									
								}
									$$=exp_result;

							}
		| L_PAR exp R_PAR { $$ = $2; }
		;

%%

int yyerror(char* msg) {
	printf("%s\n", msg);
	return 0;
}

int main (int argc, char* argv[]){
	printDebug("start parsing\n");
	return yyparse();
}