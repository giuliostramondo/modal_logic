#ifndef STRUCT_H
#define STRUCT_H

#include <malloc/malloc.h>
#include <stdio.h>
#include <assert.h>
#include "collections.h"

typedef struct t_declaration
{
	char *identifier;
	int isWorld;
	int isPredicate;
	int isEvaluation;
	int isEvaluationItem;
	int isRelation;
	t_list *elements; //list of identifiers
} t_declaration;

typedef struct t_couple
{
	char *item1;
	char *item2;
} t_couple; //for relations

typedef struct t_model
{
	char *identifier;
	t_declaration *world_set;
	t_declaration *relation;
	t_declaration *evaluation;
	t_declaration *predicates;
}t_model;

typedef struct t_expression
{
	char *world;
	t_model *model;
	t_list *worlds_results;
}t_expression;

extern t_model *alloc_model(char *identifier, t_declaration *w, t_declaration *r, t_declaration *eval,t_declaration *predicates);

extern t_couple *alloc_couple(char *item1, char *item2);

extern t_declaration *alloc_declaration(char *identifier, int isType, t_list *elements);

extern t_expression *alloc_expression(char *world, t_model *model);


#endif